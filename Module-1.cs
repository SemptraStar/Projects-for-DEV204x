﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main_App
{
    class Program
    {
        static void Main(string[] args)
        {
            String firstName, lastName, city, country, adress;

            /*
             * Using the DateTime structure to set the date of birth.
             * In line 23 'birthdate' variable sets to 01.01.0001,
             * so methods AddYear, AddMouths and AddDays should
             * send 'i - 1' double value, where 'i' is a year, month or a day
             * of user's birth.
            */

            DateTime birthdate = new DateTime();

            firstName = "Alex";
            lastName = "Green";
            city = "Kharkiv";
            country = "Ukraine";
            adress = "st. Street, 18, 44";
            birthdate = birthdate.AddYears(1997);
            birthdate = birthdate.AddMonths(7);
            birthdate = birthdate.AddDays(20);
            
            Console.WriteLine("First name: {0}", firstName);
            Console.WriteLine("Last name: {0}", lastName);
            Console.WriteLine("City: {0}", city);
            Console.WriteLine("Country: {0}", country);
            Console.WriteLine("Adress: {0}", adress);
            Console.WriteLine("Birthdate: {0}", birthdate.Date);
            Console.ReadLine();
        }
    }
}
